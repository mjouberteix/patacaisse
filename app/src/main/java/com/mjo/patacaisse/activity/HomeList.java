package com.mjo.patacaisse.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mjo.patacaisse.FaultFileHelper;
import com.mjo.patacaisse.FrenchFault;
import com.mjo.patacaisse.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class HomeList extends Activity {

    private ListView mainListView ;
    private ArrayAdapter<FrenchFault> listAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_list);

        // Find the ListView resource.
        mainListView = (ListView) findViewById( R.id.mainListView );

        final List<FrenchFault> frenchFaultList = FaultFileHelper.getFrenchFaults();

        try {
            listAdapter = new ArrayAdapter<FrenchFault>(this, android.R.layout.simple_list_item_1, frenchFaultList);
        }
        catch (Exception e)
        {
            Log.e("OUPS","Did not manage to populate the list");
        }

        // Set the ArrayAdapter as the ListView's adapter.
        mainListView.setAdapter(listAdapter);

        // listening to single list item on click
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // selected item
                String sFaultId = frenchFaultList.get(position).getId();

                // Launching new Activity on selecting single List Item
                Intent i = new Intent(getApplicationContext(), ViewFault.class);
                // sending data to new activity
                i.putExtra("FaultId", sFaultId);
                startActivity(i);
                //finish();

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_first, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add_fault) {
            Intent newActivity = new Intent(this, CreateFault.class);
            startActivity(newActivity);
            //finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Quitter")
                .setMessage("Etes-vous sür de vouloir quitter Patacaisse?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }).setNegativeButton("Non", null).show();
    }

}
