package com.mjo.patacaisse.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.mjo.patacaisse.FaultFileHelper;
import com.mjo.patacaisse.FaultHolder;
import com.mjo.patacaisse.FrenchFault;
import com.mjo.patacaisse.R;

public class ViewFault extends Activity {

    private String m_fault_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Oups! La boulette");
        setContentView(R.layout.activity_view_fault);

        TextView aTxtTitle = (TextView) findViewById(R.id.fault_title);
        TextView aTxtDesc = (TextView) findViewById(R.id.fault_description);
        TextView aTxtFee = (TextView) findViewById(R.id.fault_fee);
        TextView aTxtPaidAmount = (TextView) findViewById(R.id.fault_paid_amount);

        Intent i = getIntent();
        String sFaultId = i.getStringExtra("FaultId");
        this.m_fault_id = sFaultId;

        FrenchFault aFrenchFault = FaultHolder.getFaultHolder().getFaultMap().get(sFaultId);

        aTxtTitle.setText(aFrenchFault.getName());
        aTxtDesc.setText(aFrenchFault.getDesc());

        if (aFrenchFault.getFee() == 20)
        {
            aTxtFee.setText(" 20 Centimes");
            aTxtFee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.twenty_cent, 0, 0, 0);
        }
        else
        {
            aTxtFee.setText(" 10 Centimes");
            aTxtFee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ten_cent, 0, 0, 0);
        }
        aTxtPaidAmount.setText(getPaidAmountText(FaultFileHelper.getAlreadyPaidAmount(aFrenchFault)));

    }


    public String getPaidAmountText (int value)
    {
        if(value == 0)
        {
            return "Alex n'a jamais payé pour cette erreur!";
        }
        if(value < 100)
        {
            return "Alex a déjà payé "+value+" Centimes!";
        }
        if(value >= 100)
        {
            String centimes = (value % 100) > 0 ? " "+(value % 100)+" Centimes!" : "!";
            return "Alex a déjà payé "+value/100+" Euro"+centimes;
        }
        return "";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_fault, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent newActivity = new Intent(this, CreateFault.class);
            newActivity.putExtra("FaultId", this.m_fault_id);
            startActivity(newActivity);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getCash(View view) {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Encaisser")
                .setMessage("Voulez-vous vraiment encaisser la taxe?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FrenchFault aFrenchFault = FaultHolder.getFaultHolder().getFaultMap().get(ViewFault.this.m_fault_id);
                        FaultFileHelper.addPaidAmount( aFrenchFault);
                        Intent newActivity = new Intent(ViewFault.this, HomeList.class);
                        startActivity(newActivity);
                       ViewFault.this.finish();
                    }
                }).setNegativeButton("Non", null).show();

        ;
    }


}
