package com.mjo.patacaisse.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mjo.patacaisse.FaultFileHelper;
import com.mjo.patacaisse.FaultHolder;
import com.mjo.patacaisse.FrenchFault;
import com.mjo.patacaisse.R;


public class CreateFault extends Activity {

    private int nFee = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_fault);
        setTitle("Créer une boulette");
        TextView aTxtId = (TextView) findViewById(R.id.fault_id);
        TextView aTxtTitle = (TextView) findViewById(R.id.fault_title);
        TextView aTxtDesc = (TextView) findViewById(R.id.fault_description);
        RadioButton aTxtFeeTen = (RadioButton) findViewById(R.id.radio_ten_cent);
        RadioButton aTxtFeeTwenty = (RadioButton) findViewById(R.id.radio_twenty_cent);

        aTxtFeeTen.setChecked(true);
        aTxtFeeTwenty.setChecked(false);

        Intent i = getIntent();
        String sFaultId = i.getStringExtra("FaultId");
        if (sFaultId != null)
        {
            setTitle("Éditer la boulette");
            aTxtId.setText(sFaultId);
            aTxtId.setEnabled(false);
            FrenchFault aFrenchFault = FaultHolder.getFaultHolder().getFaultMap().get(sFaultId);

            aTxtTitle.setText(aFrenchFault.getName());
            aTxtDesc.setText(aFrenchFault.getDesc());

            if (aFrenchFault.getFee() == 20)
            {
                aTxtFeeTen.setChecked(false);
                aTxtFeeTwenty.setChecked(true);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_fault, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {


            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveNewFault(View view) {

        String sId = ((EditText) findViewById(R.id.fault_id)).getText().toString();
        String sName = ((EditText) findViewById(R.id.fault_title)).getText().toString();
        if (sId.isEmpty() || sName.isEmpty())
        {
            return;
        }
        String sDesc = ((EditText) findViewById(R.id.fault_description)).getText().toString();
        sDesc = (sDesc.isEmpty()) ? FrenchFault.EMPTY_DESC : sDesc;
        FrenchFault aFault = new FrenchFault(sId,sName,this.nFee,sDesc);
        FaultFileHelper.addFaultToFile( aFault);
        Intent newActivity = new Intent(this, HomeList.class);
        startActivity(newActivity);
        finish();
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_ten_cent:
                if (checked)
                    this.nFee = 10;
                    break;
            case R.id.radio_twenty_cent:
                if (checked)
                    this.nFee = 20;
                    break;
        }
    }
}
