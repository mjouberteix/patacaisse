package com.mjo.patacaisse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Melanie on 22/10/2014.
 */
public class FaultHolder {

        /** Constructeur privé */
        private FaultHolder()
        {
            this.m_fault_map = new HashMap<String, FrenchFault>();
        }

        /** Instance unique non préinitialisée */
        private static FaultHolder INSTANCE = null;
        private Map<String,FrenchFault> m_fault_map = null;

        /** Point d'accès pour l'instance unique du singleton */
        public static FaultHolder getFaultHolder()
        {
            if (INSTANCE == null)
            {
                INSTANCE = new FaultHolder();
            }
            return INSTANCE;
        }

        public Map<String,FrenchFault> getFaultMap()
        {
            return this.m_fault_map;
        }

        public void addFault(FrenchFault aFault)
        {
            this.m_fault_map.remove(aFault.getId());
            this.m_fault_map.put(aFault.getId(),aFault);
        }
    }

