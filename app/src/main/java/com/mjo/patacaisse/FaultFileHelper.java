package com.mjo.patacaisse;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Melanie on 20/10/2014.
 */
public class FaultFileHelper {

    public static final String FAULT_FILENAME = "FaultFile.txt";
    public static final String PAYMENT_FILENAME = "PaymentFile.txt";
    public static final String DIRECTORY_NAME =  "/patacaisse";

    public static void addFaultToFile ( FrenchFault aFault)
    {
        FileInputStream inputStream;
        FileOutputStream outputStream;
        String sFault = aFault.getId()+";"+aFault.getName()+";"+aFault.getFee()+";"+aFault.getDesc()+";"+"\n";
        String sFileContent = "";
        boolean bFound = false;
        checkExternalMedia();
        try
        {
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File (root.getAbsolutePath() + DIRECTORY_NAME);
            dir.mkdirs();

            File file = new File(dir, FAULT_FILENAME);
            inputStream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                if(line.split(";")[0].equals(aFault.getId()))
                {
                   //Update line if already existing
                    sFileContent += sFault;
                    bFound = true;
                }
                else
                {
                   sFileContent += line + "\n";
                }
            }
            br.close();
        }
        catch (FileNotFoundException e)     {             //If the file is not yet existing then the fault was for sure never added -> Do Nothing
        }
        catch (Exception e)         {            e.printStackTrace();        }

        sFileContent = bFound ? sFileContent : sFileContent + sFault;

        try {
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File (root.getAbsolutePath()  + DIRECTORY_NAME );
            boolean bCreated = dir.mkdirs();
            File file = new File(dir, FAULT_FILENAME);
            outputStream = new FileOutputStream(file);
            outputStream.write(sFileContent.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FaultHolder.getFaultHolder().addFault(aFault);
    }

    public static List<FrenchFault> getFrenchFaults()
    {
        List<FrenchFault> aList = new LinkedList<FrenchFault>();
        FileInputStream inputStream;

        try
        {
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File (root.getAbsolutePath() + DIRECTORY_NAME);
            dir.mkdirs();
            File file = new File(dir, FAULT_FILENAME);
            inputStream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                String[] aLine = line.split(";");
                int nFee = aLine[2].equals("20") ? 20 : 10;
                FrenchFault aFault = new FrenchFault(aLine[0], aLine[1], nFee, aLine[3]);
                aList.add(aFault);
                FaultHolder.getFaultHolder().addFault(aFault);
            }
            br.close();
        }
        catch (FileNotFoundException e)
        {
            //If the file is not yet existing then the fault was for sure never added -> Do Nothing
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return aList;
    }


    public static int getAlreadyPaidAmount (FrenchFault aFault)
    {
        FileInputStream inputStream;
        int nFee = 0;
        try
        {
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File (root.getAbsolutePath() + DIRECTORY_NAME);
            dir.mkdirs();
            File file = new File(dir, PAYMENT_FILENAME);
            inputStream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                String[] aValue = line.split(";");
                if(aValue[1].equals(aFault.getId()))
                {
                    int nTempFee = (aValue[2].equals("20")) ? 20 : 10;
                    nFee += nTempFee;
                }
            }
            br.close();
        }
        catch (FileNotFoundException e)
        {
            //If the file is not yet existing then the fault was for sure never added -> Do Nothing
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return nFee;
    }

    public static void addPaidAmount (FrenchFault aFault)
    {
        FileOutputStream outputStream;
        String now =  (new Long(new Date().getTime())).toString();
        String sPayment = now+";"+aFault.getId()+";"+aFault.getFee()+";\n";
        try {
            File root = android.os.Environment.getExternalStorageDirectory();
            File dir = new File (root.getAbsolutePath() + DIRECTORY_NAME);
            dir.mkdirs();
            File file = new File(dir, PAYMENT_FILENAME);
            outputStream = new FileOutputStream(file,true);
            outputStream.write(sPayment.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void checkExternalMedia(){
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        Log.d("VERIFYMEDIA","External Media: readable="
                +mExternalStorageAvailable+" writable="+mExternalStorageWriteable);
    }

}
