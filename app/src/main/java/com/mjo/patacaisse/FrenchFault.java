package com.mjo.patacaisse;

/**
 * Created by Melanie on 20/10/2014.
 */
public class FrenchFault {

    public static final String EMPTY_DESC = "Insérer une description";

    private final String m_id;
    private String m_name;
    private String m_desc;
    private int m_fee;

    public FrenchFault(String id, String sName) {
        this.m_id = id.toLowerCase();
        this.m_name = sName;
        this.m_desc = EMPTY_DESC;
        this.m_fee = 10;
    }

    public FrenchFault(String id, String sName,int nFee, String sDesc) {
        this.m_id = id.toLowerCase();
        this.m_name = sName;
        this.m_desc = sDesc;
        this.m_fee = nFee;
    }

    public String getId ()
    {
        return this.m_id;
    }

    public String getName()
    {
        return this.m_name;
    }
    public void setName(String sName) {
        this.m_name = sName;
    }

    public String getDesc()
    {
        return this.m_desc;
    }
    public void setDesc(String sDesc) {
        this.m_desc = sDesc;
    }

    public int getFee()
    {
        return this.m_fee;
    }
    public void setFee(int nFee) {
        this.m_fee = nFee;
    }

    public String toString()
{
    return this.m_name;
}

}
